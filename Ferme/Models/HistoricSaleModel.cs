﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ferme.Models
{
    public class HistoricSaleModel
    {
        public List<ProductModel> Productos { get; set; }
        public string Documento { get; set; }
        public string MedioPago { get; set; }
        public string FechaCompra { get; set; }
        public long IdVenta { get; set; } 
    }
}
