using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using Ferme.Models;
using FermeBackend;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ferme.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SaleController : ControllerBase{
        private IHttpClientFactory _clientFactory;
        public SaleController(IHttpClientFactory clientFactory){
            _clientFactory = clientFactory;
        }
        [HttpGet]
        [Route("user")]
        public async Task<List<HistoricSaleModel>> GetSalesByLoggedUser()
        {
            var historicSales = new List<HistoricSaleModel>();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            api_docsClient clienteAPI = new api_docsClient(_clientFactory.CreateClient("FermeBackendClient"));
            var userId = long.Parse(identity.FindFirst("Id").Value);
            var sales = ((JArray) await clienteAPI.GetSalesByUserIdUsingGETAsync(userId)).ToObject<List<SaleEntity>>();
            foreach (var sale in sales)
            {
                var historicSale = new HistoricSaleModel();
                historicSale.IdVenta = sale.Document.Id.Value;
                historicSale.Documento = sale.Document.Document_type.Document_type;
                historicSale.FechaCompra = sale.Document.Document_date.Value.ToString("dd/MM/yyyy");
                historicSale.MedioPago = sale.Payment_method.Payment_method;

                var Productos = new List<ProductModel>();
                foreach (var product in sale.Products_sale)
                {
                    var historicProduct = new ProductModel();
                    historicProduct.CodigoProducto = product.Product.ProductCode.Value;
                    historicProduct.Nombre = product.Product.Name;
                    historicProduct.Precio = product.Product.Price.Value;
                    historicProduct.MarcaProducto = product.Product.BrandProduct;
                    historicProduct.Cantidad = product.Quantity.Value;
                    historicProduct.Imagen = product.Product.ProductImage;
                    Productos.Add(historicProduct);
                }
                historicSale.Productos = Productos;
                historicSales.Add(historicSale);
            }
            var orderedSales = from h in historicSales orderby h.IdVenta descending select h;
            return orderedSales.ToList();
        }
        
        [HttpPost]
        [Route("Sale")]
        public async Task<String> Sale(SaleModel sale){
            try{
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                api_docsClient clienteAPI = new api_docsClient(_clientFactory.CreateClient("FermeBackendClient"));
                List<ProductSaleEntity> productsSalesApi = new List<ProductSaleEntity>();
                foreach (var product in sale.Cart){
                    ProductSaleEntity productSaleAPI = new ProductSaleEntity();
                    productSaleAPI.Product = new ProductEntity{
                        Id = product.Id
                    };
                    productSaleAPI.Quantity = product.Cantidad;
                    productsSalesApi.Add(productSaleAPI);
                }
                SaleEntity saleApi = new SaleEntity
                {
                    Products_sale = productsSalesApi,
                    User = new UserEntity
                    {
                        Id=long.Parse(identity.FindFirst("Id").Value)
                    },
                    Document = new DocumentEntity
                    {
                        Document_type = new DocumentTypeEntity
                        {
                            Id = int.Parse(sale.DocumentType)
                        }
                    },
                    Payment_method = new PaymentMethodEntity
                    {
                        Id = 1
                    }
                };
                Console.Out.WriteLine(JsonConvert.SerializeObject(saleApi));
                await clienteAPI.RecordedSaleUsingPOSTAsync(saleApi);
            }
            catch(Exception ex){
                Console.Out.WriteLine(ex.Message);
                throw (ex);
            }
            
            return JsonConvert.SerializeObject("OK");
        }
    }
}