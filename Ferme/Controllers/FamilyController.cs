using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FermeBackend;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Ferme.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FamilyController : ControllerBase
    {
        private IHttpClientFactory _clientFactory;
        public FamilyController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ProductFamilyEntity> ObtenerFamiliaPorId(long id)
        {
            api_docsClient clienteAPI = new api_docsClient(_clientFactory.CreateClient("FermeBackendClient"));
            var familias = ((JArray)(await clienteAPI.GetProductsFamilyUsingGETAsync())).ToObject<List<ProductFamilyEntity>>();
            var familia = (from ProductFamilyEntity f in familias where f.Id == id select f).FirstOrDefault();
            return familia;
        }

    }
}