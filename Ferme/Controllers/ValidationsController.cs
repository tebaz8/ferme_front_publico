using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ferme.Utils;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Ferme.Controllers
{
    [Controller]
    public class ValidationsController : ControllerBase
    {
        [AcceptVerbs("GET", "POST")]
        public JsonResult validateRut([Bind(Prefix ="Input.Rut")] string rut)
        {
            var rutSplit = rut!=null?rut.Split("-"):new[] {""};
            if (rutSplit.Length != 2) return new JsonResult("Formato de rut inválido");
            int rutInt;
            if (int.TryParse(rutSplit[0], out rutInt) && FermeUtils.CalcularDigitoVerificador(rutInt) == rutSplit[1])
            {
                return new JsonResult(true);
            }
            else return new JsonResult("Dígito verificador incorrecto");
        }
    }
}