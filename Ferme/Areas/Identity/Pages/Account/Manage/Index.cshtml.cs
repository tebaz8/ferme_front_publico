using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Ferme.Controllers;
using Ferme.IdentityProvider;
using FermeBackend;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis;
using Newtonsoft.Json.Linq;

namespace Ferme.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<FermeUser> _userManager;
        private readonly SignInManager<FermeUser> _signInManager;
        private readonly IHttpClientFactory _clientFactory;


        public IndexModel(
            UserManager<FermeUser> userManager,
            SignInManager<FermeUser> signInManager,
            IHttpClientFactory clientFactory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _clientFactory = clientFactory;
        }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [EmailAddress(ErrorMessage = "Debes ingresar un correo válido.")]
            [Required(ErrorMessage = "Debes ingresar un correo electrónico.")]
            [Display(Name = "Correo Electrónico")]
            public string Username { get; set; }

            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
            
            [Required(ErrorMessage = "Debes ingresar un nombre.")]
            [Display(Name = "Nombres")]

            public string FirstName { get; set; }

            [Required(ErrorMessage = "Debes ingresar un apellido paterno.")]
            [Display(Name = "Apellido Paterno")]
            public string LastName { get; set; }

            [Display(Name = "Apellido Materno")]
            public string SecondSurname { get; set; }

            [Required(ErrorMessage = "Debes ingresar cédula de identidad.")]
            [StringLength(10, ErrorMessage = "Existe un error en la cantidad de carácteres.", MinimumLength = 3)]
            [Display(Name = "Rut")]
            [Remote(action: "validateRut", controller: "validations")]
            public string Rut { get; set; }

            [Display(Name = "Fecha de Nacimiento")]
            public System.DateTimeOffset? BirthDate { get; set; }

            public int Phone { get; set; }
            public string Address { get; set; }
            public string Location { get; set; }
            public string Region { get; set; }
            public string City { get; set; }
            public List<SelectListItem> Regions { get; set; }
            public List<SelectListItem> Cities { get; set; }
            public List<SelectListItem> Locations { get; set; }
        }
        public async Task<List<SelectListItem>> loadRegions()
        {
            api_docsClient clientAPI = new api_docsClient(_clientFactory.CreateClient("FermeBackendClient"));
            var regiones = ((JArray)await clientAPI.GetRegionUsingGETAsync()).ToObject<List<RegionEntity>>();
            var listaRegiones = new List<SelectListItem>();
            foreach (var region in regiones)
            {
                listaRegiones.Add(new SelectListItem()
                {
                    Text = region.RegionName,
                    Value = region.Id.ToString()
                });
            }
            return listaRegiones;
        }

        private async Task LoadAsync(FermeUser user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var phoneNumber = "";
            LocationController lc = new LocationController(_clientFactory);
            List<SelectListItem> regions = await loadRegions();
            List<SelectListItem> cities = await lc.ObtenerCiudadesPorRegion((int)user.Location.City.Region.Id.Value);
            List<SelectListItem> locations = await lc.ObtenerComunasPorCiudad((int)user.Location.City.Id.Value);
            var userSplit = user.LastName.Split(" ");

            Input = new InputModel
            {
                Username = userName,
                PhoneNumber = phoneNumber,
                FirstName = user.FirstName,
                LastName = userSplit[0],
                SecondSurname = userSplit.Length > 1 ? userSplit[1] : "",
                Rut = user.Rut + "-" + user.Dv,
                BirthDate = user.BirthDate,
                Location = user.Location.Id.ToString(),
                Region = user.Location.City.Region.Id.ToString(),
                City = user.Location.City.Id.ToString(),
                Regions = regions,
                Cities = cities,
                Locations = locations,
                Address = user.Address
            };
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"No se pudo cargar usuario con ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"No se pudo cargar usuario con ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            var rutSplit = Input.Rut.Split("-");
            user.FirstName = Input.FirstName;
            user.LastName = Input.LastName;
            user.SecondSurname = Input.SecondSurname;
            user.Rut = int.Parse(rutSplit[0]);
            user.Dv = rutSplit[1];
            user.BirthDate = Input.BirthDate;
            user.Address = Input.Address;
            user.Block = "";
            user.Email = Input.Username;
            user.Location = new LocationEntity
            {
                Id=int.Parse(Input.Location),
                City = new CityEntity
                {
                    Id = int.Parse(Input.City),
                    Region = new RegionEntity
                    {
                        Id = int.Parse(Input.Region)
                    }
                }
            };
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                await _signInManager.RefreshSignInAsync(user);
                StatusMessage = "Su perfil ha sido actualizado";
            }
            else StatusMessage = "Error al actualizar usuario";
            return RedirectToPage();
        }
    }
}